import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import App from './App.vue';
import CluesRoute from './clues/CluesRoute.vue';
import IntroductionRoute from './clues/IntroductionRoute.vue';
import ClueOneRoute from './clues/ClueOneRoute.vue';
import ClueTwoRoute from './clues/ClueTwoRoute.vue';
import ClueThreeRoute from './clues/ClueThreeRoute.vue';
import FinishedRoute from './clues/FinishedRoute.vue';

const app = createApp(App);

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      name: 'clues',
      path: '/',
      component: CluesRoute,
      redirect: { name: 'introduction' },
      children: [
        {
          name: 'introduction',
          path: 'introduction',
          component: IntroductionRoute
        },
        {
          name: 'clue-one',
          path: 'one',
          component: ClueOneRoute,
        },
        {
          name: 'clue-two',
          path: 'two',
          component: ClueTwoRoute,
        },
        {
          name: 'clue-three',
          path: 'three',
          component: ClueThreeRoute,
        },
        {
          name: 'finished',
          path: 'finished',
          component: FinishedRoute,
        }
      ],
    },
  ],
});

app.use(router);

app.mount('#app');
